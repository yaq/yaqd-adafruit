# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/).

## [Unreleased]

### Fixed
- document known hardware

## [2022.2.0]

### Added
- initial release

[Unreleased]: https://gitlab.com/yaq/yaqd-adafruit/-/compare/v2022.2.0...main
[2022.2.0]: https://gitlab.com/yaq/yaqd-adafruit/-/tags/v2022.2.0
